# -*- coding: utf-8 -*-
"""Setup file for pypack package

To install, run following command in Anaconda prompt/terminal:
    pip install -e .
"""


from setuptools import setup

setup(name='pypack',  # name of package on import
      version='0.1',  # package version
      description='Demo package for teaching packaging',  # brief description
      url='https://gitlab.windenergy.dtu.dk/python-at-risoe/my-cool-package',  # git repo url
      author='Jenni Rinker',  # author(s)
      author_email='rink@dtu.dk',  # email
      license='GNU GPL',  # licensing
      packages=['pypack'],  # names of folders
      zip_safe=True)  # package can be installed from zip file
